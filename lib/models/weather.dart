import 'package:equatable/equatable.dart';

class Weather extends Equatable {
  final String weatherStateName;
  final String weatherStateAbbr;
  final String created;
  final double minTemp;
  final double maxTemp;
  final double theTemp;
  final String title;
  final String image;

  Weather({
    required this.weatherStateName,
    required this.weatherStateAbbr,
    required this.created,
    required this.minTemp,
    required this.maxTemp,
    required this.theTemp,
    required this.title,
    required this.image,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    final condition = json['current'];
    final location = json['location'];

    return Weather(
      weatherStateName: location['name'],
      weatherStateAbbr: location['tz_id'],
      created: location['localtime'],
      theTemp: condition['temp_c'],
      minTemp: condition['temp_c'] - 20,
      maxTemp: condition['temp_c'] + 20,
      title: condition['condition']['text'],
      image: condition['condition']['icon'],
    );
  }

  factory Weather.initial() => Weather(
        weatherStateName: '',
        weatherStateAbbr: '',
        created: '',
        minTemp: 100.0,
        maxTemp: 100.0,
        theTemp: 100.0,
        title: '',
        image: '',
      );

  @override
  List<Object> get props {
    return [
      weatherStateName,
      weatherStateAbbr,
      created,
      minTemp,
      maxTemp,
      theTemp,
      title,
      image,
    ];
  }

  @override
  bool get stringify => true;
}
